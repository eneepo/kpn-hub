import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import vuetify from './plugins/vuetify'
import store from './store/index.js'

import UserExplorer from './components/users/UserExplorer'
import UserFavorites from './components/users/UserFavorites'

Vue.config.productionTip = false
Vue.use(VueRouter);


const routes = [
  { path: '/explore', component: UserExplorer },
  { path: '/favorites', component: UserFavorites }
]

const router = new VueRouter({
  routes // short for `routes: routes`
})

new Vue({
  vuetify,
  render: h => h(App),
  store,
  router
}).$mount('#app')
