export default {
  namespaced: true,
  state: {
    favoriteUsers: []
  },
  getters: {
    getUserById: (state) => (id) => {
      return state.favoriteUsers.find(user => user.id === id)
    },
    getFavUsers: (state) => {
      return state.favoriteUsers
    },
    userIsInFav: (state, getters) => (id) => {
      return getters.getUserById(id) ? true : false
    },
    users(state) {
      return state.favoriteUsers
    }
  },
  mutations: {
    addUserToFav(state, user) {
      let index = state.favoriteUsers.map((user) => user.id).indexOf(user.id);
      if (index == -1) {
        state.favoriteUsers.push(user);
      }
    },
    removeUserFromFav(state, id) {
      let index = state.favoriteUsers.map((user) => user.id).indexOf(id);
      if (index > -1) {
        state.favoriteUsers.splice(index, 1);
      }
    }
  },
  actions: {
  }
}