export default {
    userSearchUrl(state, getters, rootState) {
        const url = new URL('search/users', rootState.baseUrlGithub);
        url.searchParams.set('q', getters.searchPhrase);
        return url.href
    },
    userProfileUrl: (state, getters, rootState) => (username) => {
        const url = new URL(`users/${username}`, rootState.baseUrlGithub);
        return url.href
    },
    userRepoUrl: (state, getters, rootState) => (username) => {
        const url = new URL(`users/${username}/repos`, rootState.baseUrlGithub);
        url.searchParams.set('sort', 'updated');
        url.searchParams.append('per_page', 10);
        return url.href
    },
    searchPhrase(state) {
        return state.searchPhrase
    },
    users(state) {
        return state.searchResults.items
    },
    usersTotal(state) {
        return state.searchResults.total_count
    },
    getCurrentUser(state) {
        return state.currentUser
    },
    getCurrentUserRepos(state) {
        return state.currentUserRepos
    }
}