export default {
    updateSearchPhrase(state, phrase) {
        state.searchPhrase = phrase;
    },
    updateSearchResults(state, results) {
        state.searchResults = results;
    },
    updateCurrentUser(state, user){
        state.currentUser = user
    },
    updateCurrentUserRepos(state, repos){
        state.currentUserRepos = repos
    }
}