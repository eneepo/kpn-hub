export default {
    lookForUser({ commit, getters }) {
        const url = getters.userSearchUrl;
        fetch(url)
            .then((response) => {
                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }

                return response.json();
            })
            .then((response) => {
                commit('updateSearchResults', response);
            });
    },
    async getUserProfile({ getters, commit, dispatch  }, username){
        const url = getters.userProfileUrl(username);
        fetch(url)
            .then((response) => {
                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }
                return response.json();
            })
            .then((response) => {
                commit('updateCurrentUser', response);
                dispatch('getUserRepos', response.login);
            });
    },
    async getUserRepos({ getters, commit }, username){
        const url = getters.userRepoUrl(username);
        fetch(url)
            .then((response) => {
                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }
                return response.json();
            })
            .then((response) => {
                commit('updateCurrentUserRepos', response);
            });
    }
}