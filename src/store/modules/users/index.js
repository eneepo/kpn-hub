import mutations from "./mutations";
import actions from "./actions";
import getters from "./getters";

export default {
    namespaced: true,
    state: {
      searchPhrase: '',
      searchResults: {},
      currentUser: {},
      currentUserRepos: []
    },
    getters: getters,
    mutations: mutations,
    actions: actions
  }