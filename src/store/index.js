import Vue from 'vue'
import Vuex from 'vuex'
import usersModule from './modules/users'
import favortitesModule from './modules/favorites'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    users: usersModule,
    favorites: favortitesModule
  },
  state: {
    baseUrlGithub: 'https://api.github.com/',
    favoritesList: []
  },
  getters: {
    
  },
  mutations: {
    
  },
  actions: {

  }
})